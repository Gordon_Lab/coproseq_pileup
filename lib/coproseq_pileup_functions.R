# Functions ---------------------------------------------------------------

process_attr <- function(x) {
  df.sub <- x[colnames(x) %in% c("attribute")]
  df.sub.agg <- data.frame(attr = character(0))
  for (i in 1:nrow(df.sub)) {
    df.split <- data.frame(colsplit(data.frame(attr = strsplit(as.character(df.sub$attribute[i]), split = ";")[[1]])$attr, pattern = "=", names = c("attr", paste("value", i))))
    df.sub.agg <- merge(df.sub.agg, df.split, by = "attr", all.x = T, all.y = T)
  }
  df.sub.agg.final <- as.data.frame(t(df.sub.agg)[2:ncol(df.sub.agg),])
  colnames(df.sub.agg.final) <- t(df.sub.agg)[1,]
  rownames(df.sub.agg.final) <- NULL
  return(df.sub.agg.final)
  }

adjust_feature_coords <- function(x) {
  x$start_adj <- NA
  x$end_adj <- NA
  header_lines <- system(paste("grep -c \"sequence-region\" ", x_file, sep = ""), intern = T)[1]
  contig_data <- read.table(x_file, sep = " ", nrows = as.numeric(header_lines), quote = "", skip = 1, comment.char = "")
  contig_data[,1] <- NULL
  colnames(contig_data) <- c("contig", "start", "end")
  contig_data$length <- contig_data$end - contig_data$start
  for (i in 1:length(levels(x$seqname))) {
    #  print(i)
    if (i == 1) {
      x[x$seqname == levels(x$seqname)[i],]$start_adj <- x[x$seqname == levels(x$seqname)[i],]$start
      x[x$seqname == levels(x$seqname)[i],]$end_adj <- x[x$seqname == levels(x$seqname)[i],]$end
    } else {
      cumulative_adj <- sum(contig_data$length[1:match(levels(x$seqname)[i-1], contig_data$contig)])
      #    print(cumulative_adj)
      x[x$seqname == levels(x$seqname)[i],]$start_adj <- x[x$seqname == levels(x$seqname)[i],]$start + cumulative_adj
      x[x$seqname == levels(x$seqname)[i],]$end_adj <- x[x$seqname == levels(x$seqname)[i],]$end + cumulative_adj
    }
  }
}

